# MediaMetrics

Simple wrapper for handling `http://mediametrics.ru/data/archive/` data

## Install dependencies
```shell
pip install -r requirements/main.txt
pip install -r requirements/dev.txt
```

## How to
```shell
# load and unzip new data
python load.py
```
