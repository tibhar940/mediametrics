import re
import os
import requests
import pandas as pd
from tqdm import tqdm
from zipfile import ZipFile


MAIN_URL = 'http://mediametrics.ru/data/archive/'
DIR = 'data/mediametrics/'
FULL_LOAD = 1


def get_periods():
    re_urls = re.compile('<a href."([a-z]+)/"+>')
    urls = re.findall(re_urls, requests.get(MAIN_URL).text)
    # Where are some problems in online data
    urls = [url for url in urls if url != 'online']
    return urls


def get_zip_urls(url, countrycode='ru'):
    re_urls = re.compile('<a href."({0}.+zip)"+>'.format(countrycode))
    urls = re.findall(re_urls, requests.get(url).text)
    return urls


def update_mediametrics():
    if not os.path.exists(DIR):
        os.makedirs(DIR)

    periods = get_periods()

    # Create dir if not exists
    for period in periods:
        if period not in os.listdir(DIR):
            os.mkdir(DIR + '/{0}'.format(period))
        else:
            print('"{0}" folder already exists'.format(period))

    # load data
    files_loaded = []
    for period in periods:
        for zip_url in tqdm(get_zip_urls(MAIN_URL + period), desc='Download {0}'.format(period)):
            if zip_url not in os.listdir(DIR + '{0}'.format(period)):
                r = requests.get(MAIN_URL + period + '/' + zip_url)
                with open(DIR + '{0}/{1}'.format(period, zip_url), 'wb') as fl:
                    fl.write(r.content)
                files_loaded.append(period + '/' + zip_url)
            else:
                next

    # create dir
    for period in periods:
        if period not in os.listdir(DIR):
            os.mkdir(DIR + '/{0}_'.format(period))
        else:
            print('"{0}_" folder already exists'.format(period))

    if FULL_LOAD == 1:
        files_loaded = pd.concat(map(lambda p: p + '/' + pd.Series(os.listdir(DIR + p)), periods)).tolist()

    # unzip data
    for file_name in tqdm(files_loaded, desc='Unzip files'):
        period = file_name.split('/')[0]
        try:
            with ZipFile(DIR + '/{0}'.format(file_name), 'r') as zip_fl:
                for filename in zip_fl.namelist():
                    zip_fl.extract(filename, DIR + '/{0}_'.format(period))
        except:
            next


if __name__ == '__main__':
    update_mediametrics()
